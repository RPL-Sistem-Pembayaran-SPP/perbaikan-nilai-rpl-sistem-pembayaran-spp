import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Tabs2PageRoutingModule } from './tabs2-routing.module';

import { Tabs2Page } from './tabs2.page';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'tabs2',
    component: Tabs2Page,
    children: [
      {
        path: 'utama-siswa',
        children: [
          {
            path: '',
            loadChildren: '../utama-siswa/utama-siswa.module#UtamaSiswaPageModule'
          }
        ]
      },
      {
        path: 'chat2',
        children: [
          {
            path: '',
            loadChildren: '../chat2/chat2.module#Chat2PageModule'
          }
        ]
      },
      {
        path: 'profile2',
        children: [
          {
            path: '',
            loadChildren: '../profile2/profile2.module#Profile2PageModule'
          }
        ]
      },
      {
        path: 'setting2',
        children: [
          {
            path: '',
            loadChildren: '../setting2/setting2.module#Setting2PageModule'
          }
        ]
      },

      {
        path: 'kelas-siswa',
        children: [
          {
            path: '',
            loadChildren: '../kelas-siswa/kelas-siswa.module#KelasSiswaPageModule'
          }
        ]
      },

      {
        path: 'bimbingan-siswa',
        children: [
          {
            path: '',
            loadChildren: '../bimbingan-siswa/bimbingan-siswa.module#BimbinganSiswaPageModule'
          }
        ]
      },
    ]
  },
  {
    path: '',
    redirectTo: 'tabs2/utama',
    pathMatch: 'full'
  }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Tabs2PageRoutingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Tabs2Page],
  exports: [RouterModule]
})
export class Tabs2PageModule { }
