import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-utama-siswa',
  templateUrl: './utama-siswa.page.html',
  styleUrls: ['./utama-siswa.page.scss'],
})
export class UtamaSiswaPage implements OnInit {
  user: any;

  constructor
    (
      private auth: AuthService,
      private router: Router
    ) { }

  ngOnInit() {
    this.auth.user$.subscribe(user => {
      this.user = user;
    })
  }

  editProfile() {
    this.router.navigate(['/profile2/edit2']);
  }


}