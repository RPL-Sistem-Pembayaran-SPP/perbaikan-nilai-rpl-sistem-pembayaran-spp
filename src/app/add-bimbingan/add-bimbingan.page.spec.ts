import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddBimbinganPage } from './add-bimbingan.page';

describe('AddBimbinganPage', () => {
  let component: AddBimbinganPage;
  let fixture: ComponentFixture<AddBimbinganPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBimbinganPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddBimbinganPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
