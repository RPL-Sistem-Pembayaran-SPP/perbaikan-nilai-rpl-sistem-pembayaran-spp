import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddBimbinganPage } from './add-bimbingan.page';

const routes: Routes = [
  {
    path: '',
    component: AddBimbinganPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddBimbinganPageRoutingModule {}
