export interface Xips {
    kelas: string;
    walikelas: string;
    namasiswa: string;
    karakter: string;
    penanganan: string;
}
