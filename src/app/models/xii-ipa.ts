export interface XIIipa {
    kelas: string;
    walikelas: string;
    namasiswa: string;
    karakter: string;
    penanganan: string;
}
