import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditKelasPage } from './edit-kelas.page';

describe('EditKelasPage', () => {
  let component: EditKelasPage;
  let fixture: ComponentFixture<EditKelasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditKelasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditKelasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
