import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, Platform } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tamukelas',
  templateUrl: './tamukelas.page.html',
  styleUrls: ['./tamukelas.page.scss'],
})
export class TamukelasPage {
  user: any;

  constructor
    (
      private auth: AuthService,
      private router: Router
    ) { }

  ngOnInit() {
    this.auth.user$.subscribe(user => {
      this.user = user;
    })
  }

  editProfile() {
    this.router.navigate(['/profile/edit']);
  }

}
