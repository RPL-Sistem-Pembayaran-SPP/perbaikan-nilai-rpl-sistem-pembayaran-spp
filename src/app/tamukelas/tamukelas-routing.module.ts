import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TamukelasPage } from './tamukelas.page';

const routes: Routes = [
  {
    path: '',
    component: TamukelasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TamukelasPageRoutingModule {}
