import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-profil-pembayaran',
  templateUrl: './profil-pembayaran.page.html',
  styleUrls: ['./profil-pembayaran.page.scss'],
})
export class ProfilPembayaranPage implements OnInit {


  user: any;

  constructor
    (
      private auth: AuthService,
      private router: Router
    ) { }

  ngOnInit() {
    this.auth.user$.subscribe(user => {
      this.user = user;
    })
  }

  editProfile() {
    this.router.navigate(['/profile/edit']);
  }


}
