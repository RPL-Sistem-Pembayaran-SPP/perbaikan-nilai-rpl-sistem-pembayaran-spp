import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProfilPembayaranPage } from './profil-pembayaran.page';

describe('ProfilPembayaranPage', () => {
  let component: ProfilPembayaranPage;
  let fixture: ComponentFixture<ProfilPembayaranPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilPembayaranPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfilPembayaranPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
