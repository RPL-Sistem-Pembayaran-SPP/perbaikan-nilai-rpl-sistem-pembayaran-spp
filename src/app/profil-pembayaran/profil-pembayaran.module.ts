import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilPembayaranPageRoutingModule } from './profil-pembayaran-routing.module';

import { ProfilPembayaranPage } from './profil-pembayaran.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilPembayaranPageRoutingModule
  ],
  declarations: [ProfilPembayaranPage]
})
export class ProfilPembayaranPageModule {}
