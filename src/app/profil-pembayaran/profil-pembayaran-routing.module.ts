import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilPembayaranPage } from './profil-pembayaran.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilPembayaranPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilPembayaranPageRoutingModule {}
