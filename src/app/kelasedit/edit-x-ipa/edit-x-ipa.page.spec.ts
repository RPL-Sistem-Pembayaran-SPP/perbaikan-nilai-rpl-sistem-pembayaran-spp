import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditXIpaPage } from './edit-x-ipa.page';

describe('EditXIpaPage', () => {
  let component: EditXIpaPage;
  let fixture: ComponentFixture<EditXIpaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditXIpaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditXIpaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
