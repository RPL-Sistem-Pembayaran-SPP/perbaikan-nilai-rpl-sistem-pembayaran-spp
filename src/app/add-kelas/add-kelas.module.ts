import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddKelasPageRoutingModule } from './add-kelas-routing.module';

import { AddKelasPage } from './add-kelas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddKelasPageRoutingModule
  ],
  declarations: [AddKelasPage]
})
export class AddKelasPageModule {}
