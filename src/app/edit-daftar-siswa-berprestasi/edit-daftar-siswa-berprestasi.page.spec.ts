import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditDaftarSiswaBerprestasiPage } from './edit-daftar-siswa-berprestasi.page';

describe('EditDaftarSiswaBerprestasiPage', () => {
  let component: EditDaftarSiswaBerprestasiPage;
  let fixture: ComponentFixture<EditDaftarSiswaBerprestasiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDaftarSiswaBerprestasiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditDaftarSiswaBerprestasiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
