import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditDaftarSiswaBerprestasiPageRoutingModule } from './edit-daftar-siswa-berprestasi-routing.module';

import { EditDaftarSiswaBerprestasiPage } from './edit-daftar-siswa-berprestasi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditDaftarSiswaBerprestasiPageRoutingModule
  ],
  declarations: [EditDaftarSiswaBerprestasiPage]
})
export class EditDaftarSiswaBerprestasiPageModule {}
