import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Profile2Edit2Page } from './profile2-edit2.page';

const routes: Routes = [
  {
    path: '',
    component: Profile2Edit2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Profile2Edit2PageRoutingModule {}
