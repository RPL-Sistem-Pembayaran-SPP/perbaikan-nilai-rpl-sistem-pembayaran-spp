import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Setting2PageRoutingModule } from './setting2-routing.module';

import { Setting2Page } from './setting2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Setting2PageRoutingModule
  ],
  declarations: [Setting2Page]
})
export class Setting2PageModule {}
