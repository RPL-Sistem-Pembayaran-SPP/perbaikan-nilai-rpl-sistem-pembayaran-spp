import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BimbinganSiswaPageRoutingModule } from './bimbingan-siswa-routing.module';

import { BimbinganSiswaPage } from './bimbingan-siswa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BimbinganSiswaPageRoutingModule
  ],
  declarations: [BimbinganSiswaPage]
})
export class BimbinganSiswaPageModule {}
