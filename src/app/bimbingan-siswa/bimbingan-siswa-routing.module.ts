import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BimbinganSiswaPage } from './bimbingan-siswa.page';

const routes: Routes = [
  {
    path: '',
    component: BimbinganSiswaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BimbinganSiswaPageRoutingModule {}
