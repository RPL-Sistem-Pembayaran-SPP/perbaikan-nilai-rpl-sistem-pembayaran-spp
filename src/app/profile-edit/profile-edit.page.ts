import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.page.html',
  styleUrls: ['./profile-edit.page.scss'],
})
export class ProfileEditPage implements OnInit {
  userId: string;
  rekening: string;
  bulan: string;

  constructor
    (
      private auth: AuthService,
      private afs: AngularFirestore,
      private loadingCtrl: LoadingController,
      private toastr: ToastController,
      private router: Router
    ) { }

  ngOnInit() {
    this.auth.user$.subscribe(user => {
      this.userId = user.userId;
      this.rekening = user.rekening;
      this.bulan = user.bulan;

    })
  }

  async updateProfile() {
    const loading = await this.loadingCtrl.create({
      message: 'Updating..',
      spinner: 'crescent',
      showBackdrop: true
    });

    loading.present();

    this.afs.collection('user').doc(this.userId).set({
      'userrekening': this.rekening,
      'userbulan': this.bulan,
      'editAt': Date.now()
    }, { merge: true })
      .then(() => {
        loading.dismiss();
        this.toast('Update Berhasil!', 'berhasil');
        this.router.navigate(['profil-pembayaran']);
      })
      .catch(error => {
        loading.dismiss();
        this.toast(error.message, 'danger');
      })
  }

  async toast(message, status) {
    const toast = await this.toastr.create({
      message: message,
      color: status,
      position: 'top',
      duration: 2000
    });

    toast.present();
  } // end of toast

}
