import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'utama', pathMatch: 'full' },

  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule), canActivate: [AuthGuard] },

  { path: 'utama', loadChildren: () => import('./utama/utama.module').then(m => m.UtamaPageModule) },
  { path: 'pilihan', loadChildren: () => import('./pilihan/pilihan.module').then(m => m.PilihanPageModule) },


  //halaman guru
  { path: '', loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule) },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule) },
  { path: 'register', loadChildren: () => import('./register/register.module').then(m => m.RegisterPageModule) },
  //{ path: 'profile', loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule), canActivate: [AuthGuard]},
  { path: 'profile/edit', loadChildren: () => import('./profile-edit/profile-edit.module').then(m => m.ProfileEditPageModule), canActivate: [AuthGuard] },
  //{ path: 'utama-guru', loadChildren: () => import('./utama-guru/utama-guru.module').then( m => m.UtamaGuruPageModule)},
  //{ path: 'kelas-guru', loadChildren: () => import('./kelas-guru/kelas-guru.module').then( m => m.KelasGuruPageModule)},
  { path: 'add-kelas', loadChildren: () => import('./add-kelas/add-kelas.module').then(m => m.AddKelasPageModule) },

  //{ path: 'setting', loadChildren: () => import('./setting/setting.module').then( m => m.SettingPageModule)},
  //{ path: 'chat', loadChildren: () => import('./chat/chat.module').then( m => m.ChatPageModule)},
  //{ path: 'bimbingan-guru', loadChildren: () => import('./bimbingan-guru/bimbingan-guru.module').then( m => m.BimbinganGuruPageModule)},




  //halaman siswa
  { path: '', loadChildren: () => import('./tabs2/tabs2.module').then(m => m.Tabs2PageModule) },
  { path: 'login2', loadChildren: () => import('./login2/login2.module').then(m => m.Login2PageModule) },
  { path: 'register2', loadChildren: () => import('./register2/register2.module').then(m => m.Register2PageModule) },
  { path: 'profile2', loadChildren: () => import('./profile2/profile2.module').then(m => m.Profile2PageModule) },
  { path: 'profile2/edit2', loadChildren: () => import('./profile2-edit2/profile2-edit2.module').then(m => m.Profile2Edit2PageModule) },
  { path: 'utama-siswa', loadChildren: () => import('./utama-siswa/utama-siswa.module').then(m => m.UtamaSiswaPageModule) },
  { path: 'kelas-siswa', loadChildren: () => import('./kelas-siswa/kelas-siswa.module').then(m => m.KelasSiswaPageModule) },
  { path: 'setting2', loadChildren: () => import('./setting2/setting2.module').then(m => m.Setting2PageModule) },
  { path: 'chat2', loadChildren: () => import('./chat2/chat2.module').then(m => m.Chat2PageModule) },
  { path: 'bimbingan-siswa', loadChildren: () => import('./bimbingan-siswa/bimbingan-siswa.module').then(m => m.BimbinganSiswaPageModule) },





  //halaman tamu
  { path: 'tamu', loadChildren: () => import('./tamu/tamu.module').then(m => m.TamuPageModule) },






  { path: 'forgot-password', loadChildren: () => import('./forgot-password/forgot-password.module').then(m => m.ForgotPasswordPageModule) },
  { path: 'change-password', loadChildren: () => import('./change-password/change-password.module').then(m => m.ChangePasswordPageModule), canActivate: [AuthGuard] },



  //{ path: 'pilih', loadChildren: () => import('./pilih/pilih.module').then( m => m.PilihPageModule)},
  {
    path: 'cari',
    loadChildren: () => import('./cari/cari.module').then(m => m.CariPageModule)
  },
  {
    path: 'hasil',
    loadChildren: () => import('./hasil/hasil.module').then(m => m.HasilPageModule)
  },


  {
    path: 'add-bimbingan',
    loadChildren: () => import('./add-bimbingan/add-bimbingan.module').then(m => m.AddBimbinganPageModule)
  },


  //kelas
  { path: 'x-ipa', loadChildren: () => import('./kelas/x-ipa/x-ipa.module').then(m => m.XIpaPageModule) },

  //edit kelas
  { path: 'edit-x-ipa/:id', loadChildren: () => import('./kelasedit/edit-x-ipa/edit-x-ipa.module').then(m => m.EditXIpaPageModule) },

  //add kelas
  { path: 'add-x-ipa', loadChildren: () => import('./addkelas/add-x-ipa/add-x-ipa.module').then(m => m.AddXIpaPageModule) },

  {
    path: 'tamukelas',
    loadChildren: () => import('./tamukelas/tamukelas.module').then(m => m.TamukelasPageModule)
  },
  { path: 'daftar-siswa-berprestasi', loadChildren: () => import('./daftar-siswa-berprestasi/daftar-siswa-berprestasi.module').then(m => m.DaftarSiswaBerprestasiPageModule) },
  { path: 'add-daftar-siswa-berprestasi', loadChildren: () => import('./add-daftar-siswa-berprestasi/add-daftar-siswa-berprestasi.module').then(m => m.AddDaftarSiswaBerprestasiPageModule) },
  { path: 'edit-daftar-siswa-berprestasi/:id', loadChildren: () => import('./edit-daftar-siswa-berprestasi/edit-daftar-siswa-berprestasi.module').then(m => m.EditDaftarSiswaBerprestasiPageModule) },
  {
    path: 'profil-pembayaran',
    loadChildren: () => import('./profil-pembayaran/profil-pembayaran.module').then(m => m.ProfilPembayaranPageModule)
  },












  //{ path: '**', loadChildren: () => import('./page-not-found/page-not-found.module').then( m => m.PageNotFoundPageModule)},

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
