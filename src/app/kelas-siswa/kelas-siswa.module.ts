import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KelasSiswaPageRoutingModule } from './kelas-siswa-routing.module';

import { KelasSiswaPage } from './kelas-siswa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KelasSiswaPageRoutingModule
  ],
  declarations: [KelasSiswaPage]
})
export class KelasSiswaPageModule {}
